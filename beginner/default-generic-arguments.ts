type ApiRequest<D, M = "GET"> = {
  data: D;
  method: M;
};

// type TSConfig<T = { strict: true }> = {
//   [K in keyof T]: T[K];
// };

// or

type TSConfig<T = { strict: true }> = T;
