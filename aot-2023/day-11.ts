// https://www.typescriptlang.org/docs/handbook/utility-types.html#readonlytype
// https://github.com/microsoft/TypeScript/issues/13923
// https://ayedot.com/6246/MiniBlog/Day-11--Protect-the-List--Typehero-Advent-of-TypeScript-Challenge-
// https://twitter.com/ecyrbedev/status/1734256737224663399
// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#the-primitives-string-number-and-boolean

type SantaListProtector<T> = T extends Function
  ? T
  : // TypeScript will bypass primitive types like number and string:
    {
      readonly [K in keyof T]: SantaListProtector<T[K]>;
    };
