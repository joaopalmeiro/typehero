// https://twitter.com/ecyrbedev/status/1733531956053180566
// https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-5.html#tail-recursion-elimination-on-conditional-types
// https://github.com/microsoft/TypeScript/pull/45711#issue-987896058
// https://softwaremill.com/implementing-advanced-type-level-arithmetic-in-typescript-part-1/

// type Reverse<S extends string> = S extends `${infer First}${infer Rest}`
//   ? `${Reverse<Rest>}${First}`
//   : "";

// or

type Reverse<S extends string> = S extends `${infer Head}${infer Tail}`
  ? `${Reverse<Tail>}${Head}`
  : S;
