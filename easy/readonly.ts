// Source: https://github.com/microsoft/TypeScript/blob/v5.3.2/src/lib/es5.d.ts#L1570

type MyReadonly<T> = {
  readonly [P in keyof T]: T[P];
};
