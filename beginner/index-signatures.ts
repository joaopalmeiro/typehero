type GroceryList = {
  [item: string]: number;
};

type InappropriateActionBySituation = {
  [situation: string]: string[];
};

interface Character {
  id: number;
  name: string;
  status: string;
  species: string;
}

type CharactersById = {
  [characterId: number]: Character;
};
