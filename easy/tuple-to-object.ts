// https://youtu.be/nK6qW_NsPvc?feature=shared
// Tuple: Array with `as const`/`readonly`
// https://www.totaltypescript.com/concepts/propertykey-type

type TupleToObject<T extends readonly PropertyKey[]> = {
  [K in T[number]]: K;
};
