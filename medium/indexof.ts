// Source:
// - https://youtu.be/V-VUe_lvuBQ?feature=shared
// - https://github.com/microsoft/TypeScript/issues/27024#issuecomment-421529650
// - https://github.com/type-challenges/type-challenges/blob/main/utils/index.d.ts
// - https://github.com/sindresorhus/type-fest/blob/main/source/is-equal.d.ts

// biome-ignore format: maintain the original formatting for readability.
type IsEqual<X, Y> =
  (<T>() => T extends X ? 1 : 2) extends
  (<T>() => T extends Y ? 1 : 2) ? true : false

// `1[]`: array of ones
// `Count` or `Accumulator`
type IndexOf<T extends unknown[], U, Count extends 1[] = []> = T extends [infer Head, ...infer Tail]
  ? IsEqual<Head, U> extends true
    ? Count["length"]
    : IndexOf<Tail, U, [...Count, 1]>
  : -1;
