// Source:
// - medium/indexof.ts
// - https://twitter.com/ecyrbedev/status/1734557818445869486
// - https://ayedot.com/6247/MiniBlog/Day-12--Find-Santa--Typehero-Advent-of-TypeScript-Challenge-

// Searching left to right requires an accumulator:
// type FindSanta<T extends readonly any[], Acc extends 1[] = []> = T extends [
//   infer Head,
//   ...infer Tail,
// ]
//   ? Head extends "🎅🏼"
//     ? Acc["length"]
//     : FindSanta<Tail, [...Acc, 1]>
//   : never;

// Search from right to left:
type FindSanta<T extends readonly any[]> = T extends [...infer Tail, infer Head]
  ? Head extends "🎅🏼"
    ? Tail["length"]
    : FindSanta<Tail>
  : never;
