// https://www.reddit.com/r/typescript/comments/l17l8z/is_there_a_way_to_guard_against_empty_arrays/
// https://type-level-typescript.com/arrays-and-tuples#mixing-arrays-and-tuples
// Source: https://typehero.dev/challenge/first-of-array/solutions/97 by bautistaaa

// `...any` (no need for `...any[]`)
type First<T extends any[]> = T extends [infer F, ...any] ? F : never;
