// https://blog.ricardofilipe.com/manipulating-strings-with-ts/
// https://twitter.com/ecyrbedev/status/1733923711193932169
// https://ayedot.com/6241/MiniBlog/Day-10--Christmas-Street-Suffix-Tester--Typehero-Advent-of-TypeScript-Challenge-

// https://www.typescriptlang.org/docs/handbook/2/conditional-types.html:
// - `SomeType extends OtherType ? TrueType : FalseType;`

// type StreetSuffixTester<
//   Street extends string,
//   Suffix extends string,
// > = Street extends `${infer Prefix}${Suffix}` ? true : false;

// or

type StreetSuffixTester<
  Street extends string,
  Suffix extends string,
> = Street extends `${string}${Suffix}` ? true : false;
