// https://www.typescriptlang.org/docs/handbook/2/mapped-types.html#key-remapping-via-as
// https://www.typescriptlang.org/docs/handbook/2/template-literal-types.html
// https://www.typescriptlang.org/docs/handbook/2/template-literal-types.html#capitalizestringtype
// https://www.typescriptlang.org/docs/handbook/2/objects.html#intersection-types

type AppendGood<T> = {
  [K in keyof T as `good_${string & K}`]: T[K];
};
