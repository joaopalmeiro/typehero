const identity = <T>(value: T) => value;
// or
// const identity = <T,>(value: T) => value;

// https://www.typescriptlang.org/docs/handbook/2/generics.html#generic-parameter-defaults
const mapArray = <T, U>(arr: T[], fn: (value: T) => U) => arr.map(fn);
