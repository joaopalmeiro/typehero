// https://www.typescriptlang.org/docs/handbook/2/mapped-types.html#key-remapping-via-as
// https://www.totaltypescript.com/uses-for-exclude-type-helper#7-remove-strings-containing-a-substring-from-a-union
// https://www.totaltypescript.com/uses-for-exclude-type-helper#9-remove-strings-with-a-certain-prefixsuffix-from-a-union

// type RemoveNaughtyChildren<T> = {
//   [K in keyof T as Exclude<K, `naughty_${string}`>]: T[K];
// };

// or

// https://twitter.com/ecyrbedev/status/1733425950518297059
// https://www.typescriptlang.org/docs/handbook/utility-types.html#omittype-keys

type RemoveNaughtyChildren<T> = Omit<T, `naughty_${string}`>;
