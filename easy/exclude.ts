// Source: https://github.com/microsoft/TypeScript/blob/v5.3.2/src/lib/es5.d.ts#L1591
// https://www.typescriptlang.org/docs/handbook/utility-types.html#excludeuniontype-excludedmembers
// https://youtu.be/lzluK_25mrI?feature=shared

type MyExclude<T, U> = T extends U ? never : T;
