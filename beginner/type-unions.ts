// Example of using the `satisfies` operator:

// const getDistanceInMeters = (distance: Distance) => {
//   switch (distance.unit) {
//     case "miles":
//       return {
//         unit: "meters",
//         value: distance.value / 1609.34,
//       } satisfies Meters;
//     case "meters":
//       return {
//         unit: "meters",
//         value: distance.value,
//       } satisfies Meters;
//     case "feet":
//       return {
//         unit: "meters",
//         value: distance.value * 3.28084,
//       } satisfies Meters;
//     default:
//       // @ts-expect-error
//       throw new Error(`unrecognized unit: ${distance.unit}`);
//   }
// };

// Part 1: Meters
type Meters = {
  unit: "meters";
  value: number;
};

type Miles = {
  unit: "miles";
  value: number;
};

type Feet = {
  unit: "feet";
  value: number;
};

type Distance = Meters | Miles | Feet;

// Part 2: Position
type Position =
  | "top"
  | "topLeft"
  | "topRight"
  | "left"
  | "center"
  | "right"
  | "bottomLeft"
  | "bottom"
  | "bottomRight";
