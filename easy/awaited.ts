// Source:
// - https://youtu.be/-DQpxq3-oaU?feature=shared
// - https://typehero.dev/challenge/awaited/solutions/51

// `U extends PromiseLike<any>` is required to assert to TypeScript that `U` is a `PromiseLike`
type MyAwaited<T extends PromiseLike<any>> = T extends PromiseLike<infer U>
  ? U extends PromiseLike<any>
    ? MyAwaited<U>
    : U
  : never;
