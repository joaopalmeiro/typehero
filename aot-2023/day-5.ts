type SantasList<B extends ReadonlyArray<any>, G extends ReadonlyArray<any>> = [...B, ...G];
