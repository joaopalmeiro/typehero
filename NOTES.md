# Notes

- https://typehero.dev/@joaopalmeiro
- https://typehero.dev/tracks/typescript-foundations
- https://typehero.dev/aot-2023
- https://github.com/type-challenges/type-challenges
- https://typehero.dev/challenge/type-aliases:
  - "(...) TypeScript is a _structural type system_ (...) primitive types are _not_ something known as "opaque types". To TypeScript, everything that's an alias for a number is still a number."
- https://www.npmjs.com/package/@biomejs/biome
- https://biomejs.dev/formatter
- https://prettier.io/docs/en/options.html#print-width
- https://biomejs.dev/reference/configuration/#formatter
- https://biomejs.dev/reference/cli/#biome-format
- https://biomejs.dev/formatter/#ignoring-code
- https://typehero.dev/challenge/primitive-data-types:
  - "(...) `Number`, `String`, `Boolean`, `Symbol`, `Object`. But these refer to something quite different (i.e. JavaScript global objects) and should virtually never be used as types."
- https://typehero.dev/challenge/index-signatures:
  - "JavaScript will _coerce_ anything you store as an object key to a string."
  - "(...) TypeScript actually allows setting numeric keys (so if you replaced `: string` with `: number` in the type above). This can give you the impression that you can set object keys as numbers, when in fact you cannot."
- https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-4.html#improvements-for-readonlyarray-and-readonly-tuples: `ReadonlyArray<string>` or `readonly string[]`
- https://ayubbegimkulov.com/readonly-arrays-ts/
- https://www.typescriptlang.org/docs/handbook/2/objects.html#the-readonlyarray-type
- https://uploadthing.com/
- https://www.typescriptlang.org/docs/handbook/utility-types.html
- https://typehero.dev/challenge/generic-type-arguments:
  - "If primitive (and literal) types are data, you can think of generics as functions that operate on that data. And just like functions often need arguments, we need some way to provide inputs to generic types."
- https://typehero.dev/challenge/mapped-object-types: "`K` (for "Key") and `P` (for "Property") (...)."
- https://typehero.dev/challenge/type-unions:
  - "unions are unordered (...) if you implement hacks to try to depend on the order your tests will break across different TypeScript versions"
  - "the items in a union are unique (...) doing `1 | 1 | 2 | 3` is the same as `1 | 2 | 3`"
  - "the `never` type is an empty union"
- https://github.com/samchon/typia
- https://typia.io/docs/setup/
- https://github.com/gvergnaud/hotscript
- https://bobbyhadz.com/blog/create-table-without-header-in-markdown
- https://github.com/ts-essentials/ts-essentials
- https://ayedot.com/about-us
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array: "In JavaScript, arrays aren't primitives but are instead `Array` objects (...)" + "(...) `length` property."
- Type Challenges playlist by Michigan TypeScript: https://www.youtube.com/playlist?list=PLOlZuxYbPik180vcJfsAM6xHYLVxrEgHC
- Type Challenges playlist by Matt Pocock: https://www.youtube.com/playlist?list=PLIvujZeVDLMzSJ95Z3CkNIhpUSmC3G7S_
- https://www.npmjs.com/package/@type-challenges/utils
- https://github.com/type-challenges/type-challenges/tree/main/utils
- [Tuple Types](https://www.typescriptlang.org/docs/handbook/2/objects.html#tuple-types):
  - `type StringNumberBooleans = [string, number, ...boolean[]];`
  - `type StringBooleansNumber = [string, ...boolean[], number];`
  - `type BooleansStringNumber = [...boolean[], string, number];`

## Commands

```bash
npm install -D @biomejs/biome
```

```bash
npx @biomejs/biome format --write .
```

```bash
npx @biomejs/biome format --write . --indent-style="space"
```

## Snippets

### https://typehero.dev/challenge/generic-function-arguments

```ts
interface Row<T> {
  label: string;
  value: T;
  disabled: boolean;
}
```

Note: `plain` instead of `ts` so that no formatter changes `<T,>` to `<T>`.

```plain
const createRow = <T,>(label: string, value: T, disabled = false) => ({});
```

`<T,>` is used to disambiguate between the start of JSX and the start of a generic function.

### https://typehero.dev/challenge/indexed-types

```ts
type Question = "Have the humans delivered their ultimate cup of coffee?";
type FirstCharacter = Question[0];
```

"(...) the resultant value is just `string` and not a more specific constant value like `H` (the first character (...)). That means that while it's not a syntax error to index a string type, it also effectively _does nothing_ since it will always result in _string_."

"(...) TypeScript doesn't even check (...) the specific value you index with. You'll always get back `string`:"

```ts
type Empty = "";
type IndexEmpty = Empty[9001];
```

### https://typehero.dev/challenge/generic-type-arguments

```ts
type GroceryItem<Name, Price, InStock> = {
  name: Name;
  price: Price;
  inStock: InStock;
};
```

### https://typehero.dev/challenge/generic-type-constraints

"Function type notation must be parenthesized when used in a union type (...)."

```ts
type RowConstraints = string | number | (() => string | number);
```

### https://typehero.dev/challenge/default-generic-arguments

"(...) there's no TypeScript value you can pass that will work like `undefined` does for JavaScript default arguments:"

```ts
const greet = (name = "Stranger") => {
  console.log(`Hello ${name}!`);
};

greet(); // Hello Stranger!
greet(undefined); // Hello Stranger!
greet("Mr. Monkey"); // Hello Mr. Monkey!
```

"In TypeScript, even if you provide `never` or `unknown` or `any`, the value will be inserted instead of the default."
